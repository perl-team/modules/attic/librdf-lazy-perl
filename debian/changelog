librdf-lazy-perl (0.09-2) experimental; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Simplify rules.
    Stop build-depend on devscripts cdbs.
  * Drop obsolete lintian override regarding debhelper 9.
  * Update git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to file format 4.
    + Track only MetaCPAN URL.
    + Rewrite usage comment.
    + Use substitution strings.
  * Stop build-depend on dh-buildinfo.
  * Mark build-dependencies needed only for testsuite as such.
  * Relax to (build-)depend unversioned on librdf-ns-perl:
    Needed version satisfied even in oldstable.
  * Declare compliance with Debian Policy 4.3.0.
  * Enable autopkgtest.
  * Set Rules-Requires-Root: no.
  * Wrap and sort control file.
  * Update copyright info:
    + Extend coverage of packaging.
    + Use https protocol in Format URL.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Feb 2019 22:23:47 +0100

librdf-lazy-perl (0.09-1) experimental; urgency=low

  * Initial Release.
    Closes: bug#803077.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 26 Oct 2015 18:43:39 +0100
